import { createSelector } from "@reduxjs/toolkit";

const cartItems = (state) => state.cart.cart;

export const cartTotalSelector = createSelector(cartItems, (cartItems) =>
  cartItems.reduce((total, item) => total + item.quantity * item.price, 0)
);
