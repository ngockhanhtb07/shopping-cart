import { createSlice } from "@reduxjs/toolkit";

const cartSlice = createSlice({
  name: "cart",
  initialState: {
    cart: []
  },
  reducers: {
    addProduct: (state, action) => {
      const productId = action.payload.id;

      const productIdArr = state.cart.map((product) => product.id);

      const isExist = productIdArr.includes(productId);

      if (isExist) {
        const products = state.cart.map((product) => {
          if (product.id === productId) {
            return {
              ...product,
              quantity: product.quantity + 1
            };
          }
          return product;
        });
        state.cart = [...products];
      } else {
        state.cart.push(action.payload);
      }
    },
    decrementProduct: (state, action) => {
      const productId = action.payload.id;
      const products = state.cart.map((product) => {
        console.log(product.quantity);
        if (product.id === productId) {
          return {
            ...product,
            quantity: product.quantity > 0 ? product.quantity - 1 : 0
          };
        }
        return product;
      });
      state.cart = [...products];
    },
    removeFromCart: (state, action) => {
      const productId = action.payload.id;
      state.cart = state.cart.filter((item) => item.id != productId);
    }
  }
});

export const { addProduct, decrementProduct, removeFromCart } =
  cartSlice.actions;

export default cartSlice.reducer;
