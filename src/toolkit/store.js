import counterSlice from "./counter/counterSlice";
import cartSlice from "./shoppnig-cart/cartSlice";
import todoSlice from "./todoList/todoSlice";

const { configureStore } = require("@reduxjs/toolkit");

const store = configureStore({
  reducer: {
    counter: counterSlice,
    todo: todoSlice,
    cart: cartSlice
  }
});

export default store;
