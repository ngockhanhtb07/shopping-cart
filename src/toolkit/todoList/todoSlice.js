const { createSlice } = require("@reduxjs/toolkit");

export const todoSlice = createSlice({
  name: "todo",
  initialState: {
    todo: []
  },
  reducers: {
    addTodo: (state, action) => {
      state.todo.push(action.payload);
    },
    removeTodo: (state, action) => {
      state.todo.splice(action.payload, 1);
    }
  }
});

export const { addTodo, removeTodo } = todoSlice.actions;

export default todoSlice.reducer;
