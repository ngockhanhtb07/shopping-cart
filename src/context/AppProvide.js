import React, { createContext, useState } from "react";

export const AppContext = createContext();
const AppProvide = ({ children }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [cartItems, setCartItems] = useState([]);

  return (
    <AppContext.Provider value={{ isOpen, setIsOpen, cartItems, setCartItems }}>
      {children}
    </AppContext.Provider>
  );
};

export default AppProvide;
