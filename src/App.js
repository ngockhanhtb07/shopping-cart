import "./App.css";
import DetailCartModal from "./modal/detailCartModal";
import ShoppingCart from "./pages/shopping-cart";

function App() {
  return (
    <div className="App">
      <ShoppingCart />
      <DetailCartModal />
    </div>
  );
}

export default App;
