import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { addTodo, removeTodo } from "../../toolkit/todoList/todoSlice";

const todoList = [
  {
    id: 1,
    name: "Learn reactjs",
    description: "Easy frontend"
  },
  {
    id: 2,
    name: "Learn nodejs",
    description: "Easy frontend"
  },
  {
    id: 3,
    name: "Learn vuejs",
    description: "Easy frontend"
  }
];

const Todo = () => {
  const newTodo = useSelector((state) => state.todo.todo);
  const dispatch = useDispatch();

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "space-around",
        padding: "0 50px"
      }}
    >
      <div>
        <h1>Current Todo</h1>
        <ul>
          {todoList.map((todo, index) => (
            <li
              key={todo.id}
              style={{ cursor: "pointer", margin: 20 }}
              onClick={() => dispatch(addTodo(todo))}
            >
              {todo.name}
            </li>
          ))}
        </ul>
      </div>
      <div>
        <h1>new Todo</h1>
        <ul>
          {newTodo?.map((todo, index) => (
            <li
              key={todo.id}
              style={{ cursor: "pointer", margin: 20 }}
              onClick={() => dispatch(removeTodo(index))}
            >
              {todo.name}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default Todo;
