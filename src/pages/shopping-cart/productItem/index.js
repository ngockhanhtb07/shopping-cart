import { useSnackbar } from "notistack";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { addProduct } from "../../../toolkit/shoppnig-cart/cartSlice";

const ProductItem = ({ product }) => {
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();

  const handleAddToCart = (product) => {
    dispatch(addProduct(product));
    // enqueueSnackbar("Add To Cart Success!", { variant: "success" });
  };

  return (
    <div className="item-product">
      <div className="product-info">
        <img src={product.url} alt="img" />
        <div className="sub-info">
          <p>{product.name}</p>
          <span>${product.price}</span>
        </div>
      </div>
      <button className="actions" onClick={() => handleAddToCart(product)}>
        Add To Cart
      </button>
    </div>
  );
};

export default ProductItem;
