import React from "react";
import Scrollbars from "react-custom-scrollbars";
import { connect, useSelector } from "react-redux";
import { cartTotalSelector } from "../../../selectors/cartSelector";
import ProductItem from "../productItem";

const listProduct = [
  {
    id: 0,
    name: "MacBook m1 pro",
    url: "https://cf.shopee.vn/file/c3f3edfaa9f6dafc4825b77d8449999d_tn",
    price: 100,
    quantity: 1
  },
  {
    id: 1,
    name: "T shirt",
    url: "https://cf.shopee.vn/file/687f3967b7c2fe6a134a2c11894eea4b_tn",
    price: 99,
    quantity: 1
  },
  {
    id: 2,
    name: "Iphone 13 pro",
    url: "https://cf.shopee.vn/file/31234a27876fb89cd522d7e3db1ba5ca_tn",
    price: 40,
    quantity: 1
  },
  {
    id: 3,
    name: "Shoes",
    url: "https://cf.shopee.vn/file/74ca517e1fa74dc4d974e5d03c3139de_tn",
    price: 400,
    quantity: 1
  },
  {
    id: 4,
    name: "Camera",
    url: "https://cf.shopee.vn/file/ec14dd4fc238e676e43be2a911414d4d_tn",
    price: 55,
    quantity: 1
  }
];

const ProductList = ({ totalPrice }) => {
  return (
    <React.Fragment>
      <Scrollbars style={{ height: "80vh" }}>
        {listProduct.map((product, index) => (
          <ProductItem product={product} key={index} />
        ))}
      </Scrollbars>
      <div className="footer-container">
        <div className="totalCart">
          <h4>Total price</h4>
          <span>${totalPrice}</span>
        </div>
      </div>
    </React.Fragment>
  );
};

const mapstateToProps = (state) => {
  return {
    totalPrice: cartTotalSelector(state)
  };
};

export default connect(mapstateToProps)(ProductList);
