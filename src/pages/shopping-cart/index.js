import React, { useContext } from "react";
import "./app.scss";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import ProductList from "./productList";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import { Badge } from "@mui/material";
import { useSelector } from "react-redux";
import { AppContext } from "../../context/AppProvide";

const ShoppingCart = () => {
  const countProduct = useSelector((state) => state.cart.cart);
  const { setIsOpen } = useContext(AppContext);
  return (
    <>
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static">
          <Toolbar>
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
              sx={{ mr: 2 }}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              Shopping Cart
            </Typography>
            <Button color="inherit" onClick={() => setIsOpen(true)}>
              <Badge badgeContent={countProduct.length} color="error">
                <ShoppingCartIcon />
              </Badge>
            </Button>
          </Toolbar>
        </AppBar>
      </Box>
      {/* product lis */}
      <ProductList />
    </>
  );
};

export default ShoppingCart;
