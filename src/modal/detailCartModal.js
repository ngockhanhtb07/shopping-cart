import React, { useContext, useEffect } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Slide from "@mui/material/Slide";
import { AppContext } from "../context/AppProvide";
import { makeStyles } from "@mui/styles";
import { useDispatch, useSelector } from "react-redux";
import {
  addProduct,
  decrementProduct,
  removeFromCart
} from "../toolkit/shoppnig-cart/cartSlice";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiPaper-root": {
      width: 1300
    }
  }
}));

const DetailCartModal = () => {
  const classes = useStyles();

  const cartItems = useSelector((state) => state.cart.cart);
  const dispatch = useDispatch();

  const { isOpen, setIsOpen } = useContext(AppContext);

  const handleIncrementProduct = (product) => {
    dispatch(addProduct(product));
  };

  const handleDecrementProduct = (product) => {
    dispatch(decrementProduct(product));
  };

  const handleClose = () => {
    setIsOpen(false);
  };

  useEffect(() => {
    cartItems.forEach((item) => {
      if (item.quantity === 0) {
        dispatch(removeFromCart(item));
      }
    });
  }, [dispatch, cartItems]);

  return (
    <div>
      <Dialog
        className={classes.root}
        open={isOpen}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle>{"Use Google's location service?"}</DialogTitle>

        <DialogContent>
          {cartItems.map((product, index) => (
            <div className="item-product-cart" key={index}>
              <div className="product-info">
                <img src={product.url} alt="img" />
                <div className="sub-info">
                  <p>{product.name}</p>
                  <span>${product.price}</span>
                </div>
              </div>
              <div className="product-actions">
                <button
                  className="method"
                  onClick={() => handleIncrementProduct(product)}
                >
                  +
                </button>
                <span>{product.quantity}</span>
                <button
                  className="method"
                  onClick={() => handleDecrementProduct(product)}
                >
                  -
                </button>
              </div>
            </div>
          ))}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Disagree</Button>
          <Button onClick={handleClose}>Agree</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default DetailCartModal;
